#!/usr/bin/env python3
#
# Copyright 2017 The Australian National University
#
# This software is the result of a joint project between the Defence Science
# and Technology Group and the Australian National University. It was enabled
# as part of a Next Generation Technology Fund grant:
# see https://www.dst.defence.gov.au/nextgentechfund
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
#     Unless required by applicable law or agreed to in writing, software
#     distributed under the License is distributed on an "AS IS" BASIS,
#     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#     See the License for the specific language governing permissions and
#     limitations under the License.
#

"""
Convert a set of AFL coverage tuples to a set of MoonLight bitvectors.

Author: Hendra Gunadi, Adrian Herrera
"""

from argparse import ArgumentParser
from datetime import timedelta
import os
from time import time

from tqdm import tqdm

from moonbeam.bitvector import BitVector


def parse_args():
    """Parse command-line arguments."""
    parser = ArgumentParser(description='Convert AFL tuples to MoonLight '
                                        'bitvectors')
    parser.add_argument('-i', '--in-dir', required=True,
                        help='Input directory')
    parser.add_argument('-o', '--out-dir', required=True,
                        help='Output directory')

    return parser.parse_args()


def read_afl_tuples(file_name):
    """
    Gets the afl tuples result of calling afl-showmap.

    Args:
        file_name: file name of the result of afl-showmap.

    Returns:
        A list of afl tuples contained in the file
    """
    with open(file_name, 'r') as inf:
        lines = inf.readlines()

    return [int(line.split(':')[0]) for line in lines if line]


def main():
    """The main function."""
    args = parse_args()

    in_dir = args.in_dir
    if not os.path.isdir(in_dir):
        raise Exception('The input directory `%s` is invalid' % in_dir)
    in_dir = os.path.abspath(in_dir)

    out_dir = args.out_dir
    if not os.path.isdir(out_dir):
        raise Exception('The output directory `%s` is invalid' % out_dir)
    out_dir = os.path.abspath(out_dir)

    _, _, files = next(os.walk(in_dir))
    num_files = len(files)

    num_tuples = 0
    afl_tuple_files = dict()
    all_tuples = set()

    start_time = time()
    progbar = tqdm(desc='Reading AFL tuples', total=num_files, unit='files')
    for file_name in files:
        tuple_file = os.path.join(in_dir, file_name)
        tuples = read_afl_tuples(tuple_file)

        num_tuples += len(tuples)
        afl_tuple_files[tuple_file] = sorted(tuples)
        all_tuples |= set(tuples)
        progbar.update()
    progbar.close()

    all_tuples = sorted(list(all_tuples))

    progbar = tqdm(desc='Bitvector conversion', total=num_tuples, unit='tuples')
    for file_name, tuples in afl_tuple_files.items():
        out_path = os.path.join(out_dir, '%s.bv' % os.path.basename(file_name))
        with open(out_path, 'wb') as outf:
            bit_vector = BitVector.generate(tuples, all_tuples)
            bit_vector.save(outf)
        progbar.update(len(tuples))
    progbar.close()
    time_taken = timedelta(seconds=(time() - start_time))

    print('\nStats:')
    print('  Time taken:           %s' % time_taken)
    print('  Num. files processed: %d' % num_files)
    print('  Num. unique tuples:   %d' % len(all_tuples))
    print('  Num. tuples:          %d' % num_tuples)


if __name__ == '__main__':
    main()
