#
# Copyright 2017 The Australian National University
#
# This software is the result of a joint project between the Defence Science
# and Technology Group and the Australian National University. It was enabled
# as part of a Next Generation Technology Fund grant:
# see https://www.dst.defence.gov.au/nextgentechfund
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
#     Unless required by applicable law or agreed to in writing, software
#     distributed under the License is distributed on an "AS IS" BASIS,
#     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#     See the License for the specific language governing permissions and
#     limitations under the License.
#

"""
Represents a MoonLight-compatible bitvector.

Author: Hendra Gunadi
"""

class BitVector:
    """A MoonLight-compatible bitvector."""

    @staticmethod
    def generate(points, all_points):
        """
        Generate the bit vector.

        Args:
            points: the coverage/feature points read from a particular file.
            all_points: global view over all coverage/feature points. This is
                required to identify the position of a point in the bitvector.

        Returns:
            Bitvector representation of the given points.
        """
        bit_vector = BitVector()
        idx = 0
        idx_max = len(points)

        for point in all_points:
            if idx >= idx_max:
                bit_vector.add_bit(0)
                continue

            if point < points[idx]:
                bit_vector.add_bit(0)
                continue

            while idx < idx_max and point > points[idx]:
                idx += 1

            if idx < idx_max and point == points[idx]:
                bit_vector.add_bit(1)
            else:
                bit_vector.add_bit(0)

        return bit_vector

    def __init__(self):
        self._count = 0
        self._current_char = 0
        self._vector = b''

    def _add_char(self):
        self._vector += bytes([self._current_char])
        self._current_char = 0
        self._count = 0

    @property
    def bytes(self):
        if self._count != 0:
            self._add_char()

        return self._vector

    def add_bit(self, bit):
        if bit % 2 == 1:
            self._current_char += 1 << (7 - self._count)

        self._count += 1

        if self._count == 8:
            self._add_char()

    def save(self, out_file):
        out_file.write(self.bytes)
