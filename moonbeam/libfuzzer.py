#!/usr/bin/env python3
#
# Copyright 2020 The Australian National University
#
# This software is the result of a joint project between the Defence Science
# and Technology Group and the Australian National University. It was enabled
# as part of a Next Generation Technology Fund grant:
# see https://www.dst.defence.gov.au/nextgentechfund
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
#     Unless required by applicable law or agreed to in writing, software
#     distributed under the License is distributed on an "AS IS" BASIS,
#     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#     See the License for the specific language governing permissions and
#     limitations under the License.
#

"""
Convert a libFuzzer merge control file to a set of MoonLight bitvectors.

Author: Adrian Herrera
"""

from argparse import ArgumentParser
from collections import defaultdict
from datetime import timedelta
from itertools import chain
import os
from time import time

from tqdm import tqdm

from moonbeam.bitvector import BitVector


def parse_args():
    """Parse command-line arguments."""
    parser = ArgumentParser(description='Convert a libFuzzer merge control '
                                        'file to a set of MoonLight bitvectors')
    parser.add_argument('-i', '--control-file', required=True,
                        help='Input control file')
    parser.add_argument('-m', '--metric', choices=('coverage', 'features'),
                        default='features',
                        help='Metric to minimize. Default is `features`')
    parser.add_argument('-o', '--out-dir', required=True,
                        help='Output directory')

    return parser.parse_args()


def parse_control_file(ctl_file):
    """Parse the libFuzzer merge control file."""
    num_files = int(ctl_file.readline())
    _ = int(ctl_file.readline()) # num. files in first corpus

    files = []
    for _ in range(0, num_files):
        seed_file = ctl_file.readline().rstrip()
        files.append(seed_file)

    merge_data = defaultdict(dict)
    for line in tqdm(ctl_file, desc='Reading merge data', total=num_files * 3,
                     unit='lines'):
        line = line.rstrip().split()
        if line[0] == 'STARTED':
            idx = int(line[1])
            size = int(line[2])

            merge_data[files[idx]]['size'] = size
        elif line[0] == 'FT':
            idx = int(line[1])
            features = [int(seed_file) for seed_file in line[2:]]

            merge_data[files[idx]]['features'] = features
        elif line[0] == 'COV':
            idx = int(line[1])
            coverage = [int(c) for c in line[2:]]

            merge_data[files[idx]]['coverage'] = coverage
        else:
            raise Exception('Invalid control file contents: %s' % ' '.join(line))

    return dict(merge_data)


def main():
    """The main function."""
    args = parse_args()

    control_file = args.control_file
    if not os.path.isfile(control_file):
        raise Exception('The control file `%s` is invalid' % control_file)
    control_file = os.path.abspath(control_file)

    out_dir = args.out_dir
    if not os.path.isdir(out_dir):
        raise Exception('The output directory `%s` is invalid' % out_dir)
    out_dir = os.path.abspath(out_dir)

    metric = args.metric

    start_time = time()
    with open(control_file, 'r') as in_file:
        merge_data = parse_control_file(in_file)

    num_points = sum(len(pts[metric]) for pts in merge_data.values())
    all_points = set(chain.from_iterable(pts[metric] for pts in
                                         merge_data.values()))
    all_points = sorted(list(all_points))

    progbar = tqdm(desc='Bitvector conversion', total=num_points, unit='points')
    for seed_file, data in merge_data.items():
        points = data[metric]
        out_path = os.path.join(out_dir, '%s.bv' % os.path.basename(seed_file))

        with open(out_path, 'wb') as outf:
            bitvector = BitVector.generate(points, all_points)
            bitvector.save(outf)

        progbar.update(len(points))
    progbar.close()
    time_taken = timedelta(seconds=(time() - start_time))

    print('\nStats:')
    print('  Metric:               %s' % metric)
    print('  Time taken:           %s' % time_taken)
    print('  Num. files processed: %d' % len(merge_data))
    print('  Num. unique points:   %d' % len(all_points))
    print('  Num. points:          %d' % num_points)


if __name__ == '__main__':
    main()
