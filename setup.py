import os
from setuptools import setup, find_packages


this_dir = os.path.dirname(__file__)
with open(os.path.join(this_dir, 'README.md'), encoding='utf-8') as inf:
    long_description = inf.read()


setup(
    name='MoonBeam',
    description='Generate bitvectors for distilling fuzzing corpora with MoonLight',
    long_description=long_description,
    url='https://gitlab.anu.edu.au/lunar/moonlight',
    version='0.1',
    packages=find_packages(),
    install_requires=[
        'tqdm',
    ],
    entry_points={
        'console_scripts': [
            'moonbeam-afl=moonbeam.afl:main',
            'moonbeam-libfuzzer=moonbeam.libfuzzer:main',
        ]
    },
    classifiers=[
        'Programming Language :: Python :: 3',
        'License :: OSI Approved :: Apache Software License',
        'Operating System :: OS Independent',
        'Environment :: Console',
        'Topic :: System',
        'Topic :: Security',
    ],
)
